import java.util.Comparator;

public class SortByAverageRate implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return Double.compare(o1.getAverageOfStudentGrades(),o2.getAverageOfStudentGrades());


    }
}