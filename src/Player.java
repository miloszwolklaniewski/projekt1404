import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Player implements Comparable<Player>{

    String name;
    String surname;
    Integer age;
    Integer overal;
    Integer numerButa;
    String nazwaKlubu;

    public Player(String name, String surname, Integer age, Integer overal, Integer numerButa, String nazwaKlubu) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.overal = overal;
        this.numerButa = numerButa;
        this.nazwaKlubu = nazwaKlubu;
    }

    public Player(String name, String surname, Integer age, Integer overal) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.overal = overal;
    }

    public Integer getNumerButa() {
        return numerButa;
    }

    public void setNumerButa(Integer numerButa) {
        this.numerButa = numerButa;
    }

    public String getNazwaKlubu() {
        return nazwaKlubu;
    }

    public void setNazwaKlubu(String nazwaKlubu) {
        this.nazwaKlubu = nazwaKlubu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getOveral() {
        return overal;
    }

    public void setOveral(Integer overal) {
        this.overal = overal;
    }


    @Override
    public int compareTo(Player o) {
        return overal - o.getOveral();


    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", overal=" + overal +
                ", numerButa=" + numerButa +
                ", nazwaKlubu='" + nazwaKlubu + '\'' +
                '}';
    }

    public static void main(String[] args){
        Player p1 = new Player("milosz", "wolk",32,10, 46, "WysokieLoty");
        Player p2 = new Player("konrad", "ons", 27, 50, 45, "KopieDobre");
        Player p3 = new Player("waclaw", "brzeczyk", 50, 30, 34, "gramyWkoncu");
        Player p4 = new Player("Miko", "zdzicho", 90, 40, 67, "pieknyklub");
        Player p5 = new Player("Zdi","fri",12,45, 36, "fajnyAniol");

        List<Player> list = new ArrayList<>();
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);
        list.add(p5);

        for (Player k : list) {
            System.out.println(k);
        }
        System.out.println("==== PO SORTOWANIU");

        Collections.sort(list);
        for (Player k : list) {
            System.out.println(k);
        }

        // inny sposob to         list.stream().sorted().forEach(System.out::println);

        System.out.println("====== dotyczy klasy wiek");
        Collections.sort(list, new ComparePlayerByAge());
        for (Player k : list){
            System.out.println(k);
        }
        System.out.println("dotyczy klasy rozmiar buta");
        Collections.sort(list, new ShoeSizeComparator());
        for (Player k : list) {
            System.out.println(k);
        }

        System.out.println("dotyczy nazwy klubu");
        Collections.sort(list, new ClubNameComparator());
        for (Player x : list){
            System.out.println(x);
        }

        Random random = new Random();
        int randomInt = random.nextInt ((20 - 1) + 1) + 1;
        System.out.println(randomInt);
    }
}

