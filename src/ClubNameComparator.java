import java.util.Comparator;

public class ClubNameComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return o1.getNazwaKlubu().compareTo(o2.getNazwaKlubu());
    }
}

