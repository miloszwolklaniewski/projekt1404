import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("milosz", "wolk");
        Student student2 = new Student("agnieszka","malicka");
        Student student3 = new Student("mistrz","edukacji");

        student1.addSubjectForStudent(ClassesEnum.GEOGRAPHIC);
        student1.addGrade(ClassesEnum.GEOGRAPHIC,4);

        student2.addSubjectForStudent(ClassesEnum.MATH);
        student2.addGrade(ClassesEnum.RELIGION,5);

        student3.addSubjectForStudent(ClassesEnum.LANGUAGE);
        student3.addGrade(ClassesEnum.LANGUAGE,6);


        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        list.add(student3);

        for (Student X : list) {
            System.out.println(list);
        }
        System.out.println("==== PO SORTOWANIU");
    }
}
