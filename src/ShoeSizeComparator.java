import java.util.Comparator;

public class ShoeSizeComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return o1.getNumerButa()- o2.getNumerButa();
    }
}
