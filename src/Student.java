import java.util.*;

public class Student implements Comparable<Student> {
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", studentId=" + studentId +
                ", listOfStudentSubjects=" + listOfStudentSubjects +
                ", studentGrades=" + studentGrades +
                ", random=" + random +
                '}';
    }

    private String name;
    private String lastName;
    private Integer studentId;
    private List<ClassesEnum> listOfStudentSubjects;
    private Map<ClassesEnum, Integer> studentGrades;
    private Random random = new Random();

    public Student(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
        this.studentId = random.nextInt((99999 - 10000) + 1) + 10000;
        this.listOfStudentSubjects = new ArrayList<>();
        this.studentGrades = new HashMap<>();
    }

    public void addSubjectForStudent(ClassesEnum subject) {
        //dodajemy do listy przedmiotów listOfStudentGrades
        listOfStudentSubjects.add(subject);
    }

    public void addGrade(ClassesEnum subject, Integer subjectGrade) {
        //sprawdzenie czy student uczeszcza na dany przedmiot
        //przypisanie oceny do przedmiotu, podanie jej do mapy
        if (listOfStudentSubjects == null) {
            System.out.println("Błąd nie dodano jeszcze przedmiotów.");
            return;
        }
        if (listOfStudentSubjects.contains(subject)) {
            studentGrades.put(subject, subjectGrade);
        } else {
            System.out.println("Student nie uczęszcza na dany przedmiot.");
        }

    }

    public void showStudentSubjects() {
        //print whole listOfStudentSubjects list
        for (ClassesEnum c : listOfStudentSubjects) {
            System.out.println(c);
        }
    }


    public Double getAverageOfStudentGrades() {
        //na mapie mamy właściwośc toEntrySet , po niej iterujemy wyciągając wszyskie oceny
        // i wyciągając z nich średnią
        //ZWRACAMY WYNIK w postaci double
        int sum = 0;
        for (ClassesEnum i : studentGrades.keySet()){
            if (studentGrades.get(i) != null) {
                sum = sum + studentGrades.get(i);
            }
        }
        return (double)(sum / studentGrades.size());
    }

    public List<ClassesEnum> subjectsWithoutGrade() {
        //wyciągamy wszystkie przedmioty z mampy i zwracamy liste przedmiotow, ktore maja wartosc null
        List<ClassesEnum> result = new ArrayList<ClassesEnum>();
        for (ClassesEnum i : studentGrades.keySet()) {
            if (studentGrades.get(i) == null) {
                result.add(i);
            }
        }
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    @Override
    public int compareTo(Student o) {
        return this.lastName.compareTo(o.getLastName());
    }
}