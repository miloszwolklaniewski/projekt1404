import java.util.Comparator;

public class ComparePlayerByAge implements Comparator<Player> {


    @Override
    public int compare(Player o1, Player o2) {
        return o1.getAge() - o2.getAge();

    }
}
